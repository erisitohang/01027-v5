
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// Vue.component('example', require('./components/Example.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

$(document).ready(function(){
    $('.addToCart').on('click',function(event){
        event.preventDefault();
        cartItemAdd($(this).attr('data-sku'), $(this).attr('data-quantity'), 'add');
    });
    $('.removeFromCart').on('click',function(event){
        event.preventDefault();
        cartItemAdd($(this).attr('data-sku'), 0, 'update');
    });

    $('.addQuantity').keypress(function (event) {
        var key = event.which;
        if(key === 13)
        {
            cartItemAdd($(this).attr('data-sku'), $(this).val(), 'update');
        }
    });

    function cartItemAdd(sku, quantity, method) {
        var fd = new FormData();
        fd.append( 'sku', sku );
        fd.append( 'quantity', quantity );
        fd.append( 'method', method );
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/cart/item/add',
            type: 'POST',
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            success:function(response) {
                if (method === 'update') {
                    location.reload();
                    return;
                }
                $('#cart_item_count').text(response.count);
            }
        });
    }
});