@extends('layouts.app')

@section('content')
    <div class="container">
        <form class="form-horizontal" action="/checkout" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="cart_id" value="{{$cart->getId()}}">
            <div class="col-md-6 col-md-offset-3">
                <!--REVIEW ORDER-->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Review Order <div class="pull-right"><small><a class="afix-1" href="/cart">Edit Cart</a></small></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        $total = 0;
                        $subtotal = 0;
                        ?>

                        @foreach($cart->getCartItems() as $item)
                            <?php
                            /** @var  \App\Entities\Product $product */
                            $product = $products[$item->getSku()];
                            $total += $product->getPrice()*$item->getQuantity();
                            ?>
                            <div class="form-group">
                                <div class="col-sm-3 col-xs-3">
                                    <img class="img-responsive" src="{{$product->getPicture()}}">
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <div class="col-xs-12">{{$product->getName()}}</div>
                                    <div class="col-xs-12"><small>Quantity:<span>{{$item->getQuantity()}}</span></small></div>
                                </div>
                                <div class="col-sm-3 col-xs-3 text-right">
                                    <h6><span>$</span>{{number_format($product->getPrice()*$item->getQuantity(), 2)}}</h6>
                                </div>
                            </div>
                            <div class="form-group"><hr></div>
                        @endforeach
                        <div class="form-group">
                            <div class="col-xs-12">
                                <strong>Order Total</strong>
                                <div class="pull-right"><span>$</span><span>{{number_format($total, 2)}}</span></div>
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                        <script
                                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                data-key="{{$stripeKey}}"
                                                data-amount="{{100*$total}}"
                                                data-name="Demo Site"
                                                data-description="Widget"
                                                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                                data-locale="auto"
                                                data-currency="sgd">
                                        </script>
                                </div>
                            </div>
                    </div>
                    <input type="hidden" name="amount" value="{{100*$total}}">
                </div>
                <!--REVIEW ORDER END-->
            </div>

        </form>
    </div>
@endsection
