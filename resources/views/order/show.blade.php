@extends('layouts.app')

@section('content')
    <div class="container">
        @if($order->getPaid())
            <form class="form-horizontal">
        @else
          <form class="form-horizontal" action="{{ route('order.payment') }}" method="post">
              {{ csrf_field() }}
              <input type="hidden" name="order_id" value="{{$order->getId()}}">
        @endif

            <div class="col-md-6 col-md-offset-3">
                <!--REVIEW ORDER-->
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('failed'))
                    <div class="alert alert-danger">
                        {{ session('failed') }}
                    </div>
                @endif
                <div class="panel panel-info">
                    <div class="panel-heading">
                         Order Summary <div class="pull-right"><small>
                                @if($order->getPaid())
                                    Paid
                                @else
                                    Unpaid
                                @endif
                            </small></div>
                    </div>
                    <div class="panel-body">
                        @foreach($order->getItems()->toArray() as $item)
                            <?php
                            /** @var  \App\Entities\Product $product */
                            $product = $item->getProduct();
                            ?>
                            <div class="form-group">
                                <div class="col-sm-3 col-xs-3">
                                    <img class="img-responsive" src="/{{$product->getPicture()}}">
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <div class="col-xs-12">{{$product->getName()}}</div>
                                    <div class="col-xs-12"><small>Quantity:<span>{{$item->getQuantity()}}</span></small></div>
                                </div>
                                <div class="col-sm-3 col-xs-3 text-right">
                                    <h6><span>$</span>{{number_format($product->getPrice()*$item->getQuantity(), 2)}}</h6>
                                </div>
                            </div>
                            <div class="form-group"><hr></div>
                        @endforeach
                        <div class="form-group">
                            <div class="col-xs-12">
                                <strong>Order Total</strong>
                                <div class="pull-right"><span>$</span><span>{{number_format($order->getTotal(), 2)}}</span></div>
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    @if(!$order->getPaid())
                                        <script
                                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                data-key="{{$stripeKey}}"
                                                data-amount="{{100*$order->getTotal()}}"
                                                data-name="Demo Site"
                                                data-description="Widget"
                                                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                                data-locale="auto"
                                                data-currency="sgd">
                                        </script>
                                    @endif

                                </div>
                            </div>
                    </div>
                </div>
                <!--REVIEW ORDER END-->
            </div>

        </form>
    </div>
@endsection
