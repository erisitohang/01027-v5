@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Date</th>
                <th scope="col">Amount</th>
                <th scope="col">Payment</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <th scope="row"><a href="{{route('order.show', $order->getId())}}">{{$order->getId()}}</a></th>
                    <td>{{$order->getCreatedAt()->format('Y-m-d')}}</td>
                    <td>${{number_format($order->getTotal(), 2)}}</td>
                    <td>
                        @if($order->getPaid())
                            Paid
                        @else
                            <a href="{{route('order.show', $order->getId())}}">Unpaid</a>
                        @endif
                    </td>
                </tr>

            @endforeach

            </tbody>
        </table>
    </div>
@endsection
