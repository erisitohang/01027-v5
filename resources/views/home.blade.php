@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach ($products->chunk(3) as $items)
        <div class="row list-group">
            @foreach ($items as $product)
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="{{asset($product->getPicture())}}" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                {{$product->getName()}}</h4>
                            <p class="group inner list-group-item-text">
                                {{$product->getDescription()}}</p>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p class="lead">
                                        ${{$product->getPrice()}}</p>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <button class="addToCart btn btn-success" data-sku="{{$product->getSku()}}" data-quantity="1">Add to cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @endforeach
        {{ $products->links() }}
    </div>
@endsection
