@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
                        <th> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $total = 0;
                    $subtotal = 0;
                    ?>
                    @foreach($cart->getCartItems() as $item)
                        <?php
                            /** @var  \App\Entities\Product $product */
                            $product = $products[$item->getSku()];
                            $total += $product->getPrice()*$item->getQuantity();
                        ?>
                        <tr>
                            <td class="col-md-6">
                                <div class="media">
                                    <a class="thumbnail pull-left" href="#"> <img class="media-object" src="{{$product->getPicture()}}" style="width: 72px; height: 72px;"> </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="#">{{$product->getName()}}</a></h4>
                                    </div>
                                </div></td>
                            <td class="col-md-1" style="text-align: center">
                                <input class="form-control addQuantity" id="productId-{{$product->getId()}}" value="{{$item->getQuantity()}}"
                                       data-sku="{{$product->getSku()}}">
                            </td>
                            <td class="col-md-1 text-center"><strong>${{$product->getPrice()}}</strong></td>
                            <td class="col-md-1 text-center"><strong>${{$product->getPrice()*$item->getQuantity()}}</strong></td>
                            <td class="col-md-1">
                                <button type="button" class="btn btn-danger removeFromCart" data-sku="{{$product->getSku()}}">
                                    <span class="glyphicon glyphicon-remove"></span> Remove
                                </button></td>
                        </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3>${{$total}}</h3></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td>
                            <a type="button" class="btn btn-default" href="/">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                            </a></td>
                        <td>
                            <a type="" class="btn btn-success" href="/checkout">
                                Checkout <span class="glyphicon glyphicon-play"></span>
                            </a>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
