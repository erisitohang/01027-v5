<?php

namespace App\Http\Requests\Cart;

use App\Entities\Product;
use Illuminate\Foundation\Http\FormRequest;
use LaravelDoctrine\ORM\Facades\EntityManager;

class CartItemStorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => 'required|string',
            'quantity' => 'required|numeric',
            'method' => 'required|string',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $repository = EntityManager::getRepository(Product::class);
            $product = $repository->findOneBy(
                array('sku' => $this->request->get('sku'))
            );
            if (!$product) {
                $validator->errors()->add('sku', 'Product not found!');
            }
        });
    }
}
