<?php

namespace App\Http\Controllers;

use App\Http\Responses\HomeIndexResponse;
use App\Services\ProductService;

class HomeController extends Controller
{

    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        return new HomeIndexResponse($this->productService);
    }
}
