<?php

namespace App\Http\Controllers;

use App\Http\Requests\Cart\CartItemStorePostRequest;
use App\Http\Responses\CartAddItemResponse;
use App\Http\Responses\CartIndexResponse;
use App\Services\CartService;
use App\Services\CartItemService;
use App\Services\ProductService;
use App\Services\UserService;

class CartController extends Controller
{

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CartItemService
     */
    private $cartItemService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * CartController constructor.
     * @param CartService $cartService
     * @param CartItemService $cartItemService
     * @param ProductService $productService
     */
    public function __construct(
        CartService $cartService,
        CartItemService $cartItemService,
        ProductService $productService
    ) {
        $this->cartService = $cartService;
        $this->cartItemService = $cartItemService;
        $this->productService = $productService;
    }

    /**
     * @return CartIndexResponse
     */
    public function index()
    {
        return new CartIndexResponse(
            $this->cartService,
            $this->cartItemService,
            $this->productService
        );
    }

    /**
     * @param CartItemStorePostRequest $request
     * @return CartAddItemResponse
     */
    public function addItem(CartItemStorePostRequest $request)
    {
        return new CartAddItemResponse(
            $this->cartService,
            $this->cartItemService,
            $this->productService
        );
    }
}
