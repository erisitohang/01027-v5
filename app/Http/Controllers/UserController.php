<?php

namespace App\Http\Controllers;

class UserController extends Controller
{

    /**
     * Show the user dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }
}