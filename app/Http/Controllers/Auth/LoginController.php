<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\CartHelper;
use App\Http\Controllers\Controller;
use App\Services\CartService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    protected $sessionId;

    protected $cartService;

    /**
     * Create a new controller instance.
     *
     * @param CartService $cartService
     *
     * @return void
     */
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        $cart = $this->cartService->merge($user->id, $this->sessionId);

        if ($cart) {
            CartHelper::setSession($request, $cart->getCartItems()->toArray());
        }
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $this->sessionId = $request->session()->getId();
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
}
