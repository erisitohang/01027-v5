<?php

namespace App\Http\Controllers;


use App\Http\Requests\OrderPaymentPostRequest;
use App\Http\Requests\OrderStorePostRequest;
use App\Http\Responses\OrderCheckoutResponse;
use App\Http\Responses\OrderIndexResponse;
use App\Http\Responses\OrderPaymentResponse;
use App\Http\Responses\OrderShowResponse;
use App\Http\Responses\OrderStoreResponse;
use App\Services\CartItemService;
use App\Services\CartService;
use App\Services\OrderItemService;
use App\Services\OrderService;
use App\Services\ProductService;
use App\Services\UserService;

class OrderController extends Controller
{

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var OrderItemService
     */
    private $orderItemService;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CartItemService
     */
    private $cartItemService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * CartController constructor.
     * @param UserService $userService
     * @param OrderService $orderService
     * @param OrderItemService $orderItemService
     * @param CartService $cartService
     * @param CartItemService $cartItemService
     * @param ProductService $productService
     */
    public function __construct(
        UserService $userService,
        OrderService $orderService,
        OrderItemService $orderItemService,
        CartService $cartService,
        CartItemService $cartItemService,
        ProductService $productService
    ) {
        $this->userService = $userService;
        $this->orderService = $orderService;
        $this->orderItemService = $orderItemService;
        $this->cartService = $cartService;
        $this->cartItemService = $cartItemService;
        $this->productService = $productService;
    }

    public function index()
    {
        return new OrderIndexResponse(
            $this->userService,
            $this->orderService
        );
    }

    public function show($id)
    {
        return new OrderShowResponse(
            $id,
            $this->orderService
        );
    }

    public function checkout()
    {
        return new OrderCheckoutResponse(
            $this->cartService,
            $this->cartItemService,
            $this->productService
        );
    }

    public function store(OrderStorePostRequest $request)
    {
        return new OrderStoreResponse(
            $this->userService,
            $this->orderService,
            $this->orderItemService,
            $this->cartService,
            $this->cartItemService,
            $this->productService
        );
    }

    public function payment(OrderPaymentPostRequest $request)
    {
        return new OrderPaymentResponse(
            $this->userService,
            $this->orderService
        );
    }
}