<?php

namespace App\Http\Responses;

use App\Entities\Cart;
use App\Helpers\CartHelper;
use App\Services\CartService;
use App\Services\CartItemService;
use App\Services\ProductService;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;

class CartIndexResponse implements Responsable
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CartItemService
     */
    private $cartItemService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var string
     */
    const CART_ITEM_COUNT = 'cart_item_count';

    /**
     * CartIndexResponse constructor.
     *
     * @param CartService $cartService
     * @param CartItemService $cartItemService
     * @param ProductService $productService
     */
    public function __construct(
        CartService $cartService,
        CartItemService $cartItemService,
        ProductService $productService
    ) {
        $this->cartService = $cartService;
        $this->cartItemService = $cartItemService;
        $this->productService = $productService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $userId = Auth::id();
        $sessionId = $request->session()->getId();

        /** @var Cart $cart */
        $cart = $this->cartService->get($userId, $sessionId);
        $skuList = CartHelper::getAllSku($cart->getCartItems()->toArray());
        $data = $this->productService->findBySkuList($skuList);
        $products = CartHelper::productTransform($data);
        return view('cart.index', compact('cart','products'));
    }
}
