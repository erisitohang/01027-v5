<?php

namespace App\Http\Responses;

use App\Helpers\CartHelper;
use App\Services\CartService;
use App\Services\CartItemService;
use App\Services\OrderService;
use App\Services\PaymentStripeService;
use App\Services\ProductService;
use App\Services\UserService;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;

class OrderPaymentResponse implements Responsable
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * OrderPayResponse constructor.
     * @param UserService $userService
     * @param OrderService $orderService
     */
    public function __construct(
        UserService $userService,
        OrderService $orderService
    ) {
        $this->userService = $userService;
        $this->orderService = $orderService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function toResponse($request)
    {
        $userId = Auth::user()->id;
        $token = $request->input('stripeToken');
        $orderId = $request->input('order_id');
        $user = $this->userService->getById(Auth::user()->id);
        $order = $this->orderService->findById($orderId, $userId);

        $items = $order->getItems()->toArray();
        $products = $this->getProducts($items);

        $payment = new PaymentStripeService($user);
        $payment->setToken($token);
        $payment->setItems($items);
        $payment->setProducts($products);
        $pay = $payment->pay($order);
        if ($payment->isStatus()) {
            $this->orderService->setAsPaid($order);
            return redirect()
                ->route('order.show', $order->getId())
                ->with('success', 'Thanks for your purchase!');
        } else {
            return redirect()
                ->route('order.payment' , $order->getId())
                ->with('failed', 'Payment failed!');
        }
    }

    private function getProducts($items)
    {
        $products = [];
         foreach ($items as $item) {
             $products[] = $item->getProduct();
         }
    }
}
