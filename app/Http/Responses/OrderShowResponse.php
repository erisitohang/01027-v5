<?php

namespace App\Http\Responses;

use App\Helpers\CartHelper;
use App\Services\CartService;
use App\Services\CartItemService;
use App\Services\OrderService;
use App\Services\PaymentStripeService;
use App\Services\ProductService;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;

class OrderShowResponse implements Responsable
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * OrderShowResponse constructor.
     * @param int $orderId
     * @param OrderService $orderService
     */
    public function __construct($orderId, OrderService $orderService)
    {
        $this->id = $orderId;
        $this->orderService = $orderService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function toResponse($request)
    {
        $userId = Auth::id();
        $order = $this->orderService->findById($this->id, $userId);
        $stripeKey = env(PaymentStripeService::STRIPE_KEY);
        return view('order.show', compact('order', 'stripeKey'));
    }
}
