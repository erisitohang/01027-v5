<?php

namespace App\Http\Responses;

use App\Entities\CartItem;
use App\Entities\Product;
use App\Helpers\CartHelper;
use App\Services\CartService;
use App\Services\CartItemService;
use App\Services\OrderItemService;
use App\Services\OrderService;
use App\Services\PaymentStripeService;
use App\Services\ProductService;
use App\Services\UserService;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class OrderStoreResponse implements Responsable
{
    const STRIPE_SECRET = 'STRIPE_SECRET';

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var OrderItemService
     */
    private $orderItemService;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CartItemService
     */
    private $cartItemService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * OrderStoreResponse constructor.
     *
     * @param UserService $userService
     * @param OrderService $orderService
     * @param OrderItemService $orderItemService
     * @param CartService $cartService
     * @param CartItemService $cartItemService
     * @param ProductService $productService
     */
    public function __construct(
        UserService $userService,
        OrderService $orderService,
        OrderItemService $orderItemService,
        CartService $cartService,
        CartItemService $cartItemService,
        ProductService $productService
    ) {
        $this->userService = $userService;
        $this->orderService = $orderService;
        $this->orderItemService = $orderItemService;
        $this->cartService = $cartService;
        $this->cartItemService = $cartItemService;
        $this->productService = $productService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function toResponse($request)
    {
        $token = $request->input('stripeToken');
        $cartId = $request->input('cart_id');
        $amount = $request->input('amount');
        $items = $this->cartItemService->findByCartId($cartId);
        $skuList = CartHelper::getAllSku($items);
        $data = $this->productService->findBySkuList($skuList);
        $products = CartHelper::productTransform($data);
        $total = $this->getTotal($items, $products);
        $user = $this->userService->getById(Auth::user()->id);

        $saveOrder = $this->orderService->create($user, $total, 0, 0);
        $this->orderItemService->create($saveOrder, $items, $products);
        $this->cartItemService->deleteList($items);
        $request->session()->remove(CartHelper::CART_ITEM_COUNT);

        $payment = new PaymentStripeService($user);
        $payment->setToken($token);
        $payment->setItems($items);
        $payment->setProducts($products);

        $pay = $payment->pay($saveOrder);

        if ($payment->isStatus()) {
            $this->orderService->setAsPaid($saveOrder);
            return redirect()
                ->route('order.show', $saveOrder->getId())
                ->with('success', 'Thanks for your purchase!');
        } else {
            return redirect()
                ->route('order.payment' , $saveOrder->getId())
                ->with('failed', 'Payment failed!');
        }
    }

    private function getTotal($items, $products)
    {
        $total = 0;
        /** @var CartItem $item */
        foreach ($items as $item) {
            if (isset($products[$item->getSku()])) {
                /** @var Product $product */
                $product = $products[$item->getSku()];
                $total += $product->getPrice();
            }
        }

        return $total;
    }
}
