<?php

namespace App\Http\Responses;

use App\Services\OrderService;
use App\Services\UserService;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;

class OrderIndexResponse implements Responsable
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * OrderPayResponse constructor.
     * @param UserService $userService
     * @param OrderService $orderService
     */
    public function __construct(
        UserService $userService,
        OrderService $orderService
    ) {
        $this->userService = $userService;
        $this->orderService = $orderService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function toResponse($request)
    {
        $page = OrderService::PAGE;
        $limit = OrderService::LIMIT;

        if ($request->has('page')) {
            $page = $request->get('page');
        }

        if ($request->has('limit')) {
            $limit = $request->get('limit');
        }

        $userId = Auth::id();

        $orders = $this->orderService->all($userId, $limit, $page);

        return view('order.index', compact('orders'));
    }
}
