<?php

namespace App\Http\Responses;

use App\Entities\Cart;
use App\Helpers\CartHelper;
use App\Services\CartService;
use App\Services\CartItemService;
use App\Services\ProductService;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Auth;

class CartAddItemResponse implements Responsable
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var CartItemService
     */
    private $cartItemService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * CartAddItemResponse constructor.
     * @param CartService $cartService
     * @param CartItemService $cartItemService
     * @param ProductService $productService
     */
    public function __construct(
        CartService $cartService,
        CartItemService $cartItemService,
        ProductService $productService
    )
    {
        $this->cartService = $cartService;
        $this->cartItemService = $cartItemService;
        $this->productService = $productService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function toResponse($request)
    {
        $userId = Auth::id();
        $sessionId = $request->session()->getId();
        $sku = $request->get('sku');
        $quantity = $request->get('quantity');
        $method = $request->get('method');
        /** @var Cart $cart */
        $cart = $this->cartService->get($userId, $sessionId);

        if ($quantity > 0) {
            $this->cartItemService->add(
                $cart,
                $sku,
                $quantity,
                $method
            );
        } else {
            $this->cartItemService->remove($cart, $sku);
        }


        $count = CartHelper::setSession($request, $cart->getCartItems()->toArray(), $quantity);

        if ($request->ajax()) {
            return response()->json(['count' => $count]);
        }

        return back()->with(['success' => 'Congratulations!']);
    }
}
