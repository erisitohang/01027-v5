<?php

namespace App\Http\Responses;

use App\Http\Requests\HomeIndexGetRequest;
use Illuminate\Contracts\Support\Responsable;
use App\Services\ProductService;

class HomeIndexResponse implements Responsable
{
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * HomeIndexResponse constructor.
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        $page = ProductService::PAGE;
        $limit = ProductService::LIMIT;

        if ($request->has('page')) {
            $page = $request->get('page');
        }

        if ($request->has('limit')) {
            $limit = $request->get('limit');
        }

        $products = $this->productService->all($limit, $page);

        return view('home', compact('products', $products));
    }
}
