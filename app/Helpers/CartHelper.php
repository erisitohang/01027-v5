<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Request;

class CartHelper
{

    const CART_ITEM_COUNT = 'cart_item_count';

    /**
     * @param Request $request
     * @param array $items
     * @param int $quantity
     * @return int
     */
    public static function setSession($request, $items, $quantity = 0)
    {
        $count = 0;
        if (!$items) {
            $count = $quantity;
        }
        /** @var CartItem $item */
        foreach ($items as $item) {
            $count += $item->getQuantity();
        }

        $request->session()->put(self::CART_ITEM_COUNT, $count);

        return $count;
    }

    /**
     * @param array $items
     * @return array
     */
    public static function getAllSku($items)
    {
        $skuList = [];
        foreach ($items as $item) {
            if (!in_array($item->getSku(), $skuList)) {
                $skuList[] = $item->getSku();
            }
        }

        return $skuList;
    }

    /**
     * @param array $data
     * @return array
     */
    public static function productTransform($data)
    {
        $products = [];
        foreach ($data as $product)
        {
            $products[$product->getSku()] = $product;
        }

        return $products;
    }

}
