<?php

namespace App\Services;

use App\Repositories\User\UserRepositoryContract;

class UserService
{

    /**
     * @var UserRepositoryContract
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->userRepository->create(
            $data['name'],
            $data['email'],
            bcrypt($data['password'])
        );
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->userRepository->getById($id);
    }
}