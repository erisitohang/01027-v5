<?php

namespace App\Services;

use App\Entities\Order;
use App\Entities\User;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class PaymentStripeService extends PaymentService implements PaymentInterfaceService
{
    const STRIPE_SECRET = 'STRIPE_SECRET';
    const STRIPE_KEY = 'STRIPE_KEY';
    const CENT_CONVERTER = 100;

    private $token;

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function pay($order)
    {
        Stripe::setApiKey(env(static::STRIPE_SECRET));

        if (!$this->isStripeCustomer()) {
            $customer = $this->createStripeCustomer();
        } else {
            $customer = Customer::retrieve($this->user->getStripeId());
        }

        return $this->execute($customer, $order);
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    private function isStripeCustomer()
    {
        return !empty($this->user->getStripeId());
    }

    private function createStripeCustomer()
    {
        Stripe::setApiKey(env(self::STRIPE_SECRET));

        $customer = Customer::create(array(
            "description" => $this->user->getEmail(),
            "source" => $this->token
        ));

        Auth::user()->stripe_id = $customer->id;

        Auth::user()->save();

        return $customer;
    }

    /**
     * @param $customer
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse|Charge
     */
    public function execute($customer, Order $order)
    {
        try {
            $charge = Charge::create(array(
                "amount" => static::CENT_CONVERTER * $order->getTotal(),
                "currency" => "brl",
                "customer" => $customer->id,
                "description" => "Order ID#" . $order->getId()
            ));

            $this->status = true;

            return $charge;

        } catch (\Stripe\Error\Card $e) {
            return redirect()
                ->route('index')
                ->with('error', 'Your credit card was been declined. Please try again or contact us.');
        }
    }
}