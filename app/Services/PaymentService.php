<?php

namespace App\Services;

use App\Entities\CartItem;
use App\Entities\Product;
use Doctrine\Common\Collections\Collection;

class PaymentService
{
    /**
     * @var Collection
     */
   protected $items;

    /**
     * @var array
     */
   protected $products;

    /**
     * @var bool
     */
   protected $status = false;

   public function setItems($items)
   {
       $this->items = $items;
   }

    public function setProducts($products)
    {
        $this->products = $products;
    }

   public function getTotal()
   {
       /** @var CartItem $item */
       $total = 0;
       foreach ($this->items as $item) {
           if (isset($this->products[$item->getSku()])) {
               /** @var Product $product */
               $product = $this->products[$item->getSku()];
               $total += $product->getPrice();
           }
       }
   }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }
}