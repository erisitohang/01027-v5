<?php

namespace App\Services;


use App\Repositories\Cart\CartRepositoryContract;
use App\Repositories\Cart\CartItemRepositoryContract;
use Illuminate\Session\SessionManager;

class CartService
{
    /**
     * @var CartRepositoryContract
     */
    private $cartRepository;

    /**
     * @var CartItemRepositoryContract
     */
    private $cartItemRepository;

    /**
     * @var string
     */
    const CART = 'cart';

    /**
     * CartService constructor.
     *
     * @param CartRepositoryContract $cartRepository
     * @param CartItemRepositoryContract $cartItemRepository
     * @param SessionManager $session
     */
    public function __construct(
        CartRepositoryContract $cartRepository,
        CartItemRepositoryContract $cartItemRepository,
        SessionManager $session
    ) {
        $this->cartRepository = $cartRepository;
        $this->cartItemRepository = $cartItemRepository;
    }

    /**
     * @param int $userId
     * @param string $sessionId
     * @return null
     */
    public function get($userId, $sessionId)
    {
        $cart = null;

        if ($userId > 0) {
            $cart =  $this->cartRepository->findUserId($userId);
        } elseif (!empty($sessionId)) {
            $userId = 0;
            $cart =  $this->cartRepository->findSessionId($sessionId);
        }

        if (!$cart) {
            $cart =  $this->cartRepository->create($userId, $sessionId);
        }
        return $cart;
    }

    /**
     * @param int $userId
     * @param string $sessionId
     * @return null
     */
    public function merge($userId, $sessionId)
    {
        $cartUserId = $this->cartRepository->findUserId($userId);
        $cartSessionId = $this->cartRepository->findSessionId($sessionId);

        if (!$cartUserId) {
            $this->cartRepository->create($userId, $sessionId);
        }

        if (
            $cartUserId &&
            $cartSessionId &&
            $cartUserId->getId() !== $cartSessionId->getId()
        ) {
            $cart = $this->cartItemRepository->updateUser($cartSessionId, $cartUserId);

            return $cart;
        }

        return null;

    }
}
