<?php

namespace App\Services;

use App\Entities\User;
use App\Repositories\Order\OrderRepositoryContract;

class OrderService
{
    const PAGE = 1;

    const LIMIT = 10;

    private $orderRepository;

    public function __construct(OrderRepositoryContract $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param User $user
     * @param float $total
     * @param int $paid
     * @param int $shipped
     */
    public function create(User $user, float $total, int $paid, int $shipped)
    {
        return $this->orderRepository->create($user, $total, $paid, $shipped);
    }

    public function findById($orderId, $userId)
    {
        return $this->orderRepository->findById($orderId, $userId);
    }

    public function setAsPaid($order)
    {
        return $this->orderRepository->setAsPaid($order);
    }

    public function all($userId, $limit, $page)
    {
        return $this->orderRepository->all($userId, $limit, $page);
    }
}