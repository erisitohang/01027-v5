<?php

namespace App\Services;

use App\Entities\Cart;
use App\Repositories\Cart\CartItemRepositoryContract;

class CartItemService
{
    /**
     * @var CartItemRepositoryContract
     */
    private $cartItemRepository;

    public function __construct(CartItemRepositoryContract $cartItemRepository)
    {
        $this->cartItemRepository = $cartItemRepository;
    }

    /**
     *  Add an item to the cart.
     *
     * @param Cart $cart
     * @param string $sku
     * @param string $quantity
     * @param string $method
     */
    public function add($cart, string  $sku, $quantity, $method)
    {
        return $this->cartItemRepository->add($cart, $sku, $quantity, $method);
    }

    /**
     *  remove an item to the cart.
     *
     * @param Cart $cart
     * @param string $sku
     */
    public function remove($cart, string  $sku)
    {
        return $this->cartItemRepository->remove($cart, $sku);
    }

    /**
     * @param int $cartId
     * @return mixed
     */
    public function findByCartId($cartId)
    {
        return $this->cartItemRepository->findByCartId($cartId);
    }

    /**
     * @param array $items
     * @return mixed
     */
    public function deleteList($items)
    {
        return $this->cartItemRepository->deleteList($items);
    }

}
