<?php

namespace App\Services;


use App\Repositories\Product\ProductRepositoryContract;

class ProductService
{
    /**
     * @var ProductRepositoryContract
     */
    private $productRepository;

    /**
     * @var int
     */
    const PAGE = 1;

    /**
     * @var int
     */
    const LIMIT = 6;

    /**
     * ProductService constructor.
     * @param ProductRepositoryContract $productRepository
     */
    public function __construct(ProductRepositoryContract $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param int $limit
     * @param int $page
     * @return mixed
     */
    public function all($limit = self::LIMIT, $page = self::PAGE)
    {
        return $this->productRepository->all($limit, $page);
    }

    /**
     * @param string $sku
     * @return mixed
     */
    public function findBySku(string $sku)
    {
        return $this->productRepository->findSku($sku);
    }

    /**
     * @param array $list
     */
    public function findBySkuList(array $list)
    {
        return $this->productRepository->findBySkuList($list);
    }
}
