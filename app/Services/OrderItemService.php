<?php

namespace App\Services;

use App\Entities\CartItem;
use App\Entities\Order;
use App\Entities\Product;
use App\Entities\User;
use App\Repositories\Order\OrderItemRepositoryContract;
use App\Repositories\Order\OrderRepositoryContract;

class OrderItemService
{
    private $orderItemRepository;

    public function __construct(OrderItemRepositoryContract $orderItemRepository)
    {
        $this->orderItemRepository = $orderItemRepository;
    }

    /**
     * @param Order $order
     * @param $items
     * @param $products
     * @return mixed
     */
    public function create(Order $order, $items, $products)
    {
        /** @var CartItem $item */
        foreach ($items as $item) {
            /** @var Product $product */
            $product = $products[$item->getSku()];
            $this->orderItemRepository->create($order, $product, $item->getQuantity(), $product->getPrice());
        }

    }
}