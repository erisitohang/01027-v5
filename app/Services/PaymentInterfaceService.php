<?php

namespace App\Services;


interface PaymentInterfaceService
{
    public function pay($order);
}