<?php

namespace App\Providers;

use App\Entities\Cart;
use App\Entities\CartItem;
use App\Entities\Order;
use App\Entities\OrderItem;
use App\Entities\Product;
use App\Entities\User;
use App\Repositories\Cart\CartItemRepository;
use App\Repositories\Cart\CartItemRepositoryContract;
use App\Repositories\Cart\CartRepository;
use App\Repositories\Cart\CartRepositoryContract;
use App\Repositories\Order\OrderItemRepository;
use App\Repositories\Order\OrderItemRepositoryContract;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderRepositoryContract;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryContract;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductRepositoryContract::class, function($app) {
            return new ProductRepository(
                $app['em'],
                $app['em']->getClassMetaData(Product::class)
            );
        });
        $this->app->bind(CartRepositoryContract::class, function($app) {
            return new CartRepository(
                $app['em'],
                $app['em']->getClassMetaData(Cart::class)
            );
        });
        $this->app->bind(CartItemRepositoryContract::class, function($app) {
            return new CartItemRepository(
                $app['em'],
                $app['em']->getClassMetaData(CartItem::class)
            );
        });
        $this->app->bind(UserRepositoryContract::class, function($app) {
            return new UserRepository(
                $app['em'],
                $app['em']->getClassMetaData(User::class)
            );
        });



        $this->app->bind(OrderRepositoryContract::class, function($app) {
            return new OrderRepository(
                $app['em'],
                $app['em']->getClassMetaData(Order::class)
            );
        });

        $this->app->bind(OrderItemRepositoryContract::class, function($app) {
            return new OrderItemRepository(
                $app['em'],
                $app['em']->getClassMetaData(OrderItem::class)
            );
        });
    }
}
