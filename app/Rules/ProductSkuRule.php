<?php

namespace App\Rules;

use App\Repositories\Product\ProductRepositoryContract;
use Illuminate\Contracts\Validation\Rule;

class ProductSkuRule implements Rule
{
    /**
     * @var ProductRepositoryContract
     */
    private $productRepository;

    /**
     * Create a new rule instance.
     *
     * ProductSkuRule constructor.
     * @param ProductRepositoryContract $productRepository
     */
    public function __construct(ProductRepositoryContract $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $cart =  $this->productRepository->findSku($value);

        return !empty($cart);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Product not found';
    }
}
