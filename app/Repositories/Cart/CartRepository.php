<?php

namespace App\Repositories\Cart;

use App\Entities\Cart;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Doctrine\ORM\EntityRepository;

class CartRepository extends EntityRepository implements CartRepositoryContract
{

    /**
     * @param int $userId
     * @param string $sessionId
     * @return Cart
     */
    public function create($userId, $sessionId)
    {
        $cart = new Cart(
            $userId,
            $sessionId
        );

        EntityManager::persist($cart);
        EntityManager::flush();

        return $cart;
    }

    /**
     * @param int $userId
     * @return null|object
     */
    public function findUserId(int $userId)
    {
        return $this->findOneBy(['user_id' => $userId]);
    }

    /**
     * @param string $sessionId
     * @return null|object
     */
    public function findSessionId(string $sessionId)
    {
        $cart = $this->findOneBy(['session_id' => $sessionId]);
        return $cart;
    }

    /**
     * @param int $cartId
     * @return mixed
     */
    public function delete(int $cartId)
    {
        $cart =  $this->find($cartId);

        EntityManager::remove($cart);
        EntityManager::flush();

        return $cart;
    }
}
