<?php

namespace App\Repositories\Cart;

use App\Entities\Cart;

interface CartItemRepositoryContract
{
    public function add(Cart $cart, string $sku, int $quantity, string $method);
    public function updateUser($oldCart, $newCart);
    public function findByCartId($cartId);
    public function remove(Cart $cart, string $sku);
    public function deleteList($items);
}
