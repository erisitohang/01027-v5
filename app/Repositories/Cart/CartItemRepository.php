<?php

namespace App\Repositories\Cart;

use App\Entities\Cart;
use App\Entities\CartItem;
use App\Entities\Product;
use App\Services\ProductService;
use Doctrine\ORM\EntityRepository;
use Illuminate\Support\Facades\Log;
use LaravelDoctrine\ORM\Facades\EntityManager;

class CartItemRepository extends EntityRepository implements CartItemRepositoryContract
{
    const UPDATE = 'update';

    /**
     * @param Cart $cart
     * @param string $sku
     * @param int $quantity
     * @param string $method
     * @return CartItem
     */
    public function add(Cart $cart, string $sku, int $quantity, string $method)
    {
        /** @var  CartItem $cartItem */
        $cartItem = $this->findOneBy(['cart' => $cart->getId(), 'sku' => $sku]);

        if ($cartItem && $method !== self::UPDATE) {
            $quantity = $cartItem->getQuantity() + $quantity;
        }

        if ($cartItem) {
            $cartItem->setQuantity($quantity);
        } else {
            $cartItem = new CartItem (
                $cart,
                $sku,
                $quantity
            );
        }

        EntityManager::persist($cartItem);
        EntityManager::flush();

        return $cartItem;
    }

    /**
     * @param Cart $cart
     * @param string $sku
     * @return Cart|null|object
     */
    public function remove(Cart $cart, string $sku)
    {
        $cart =  $this->findOneBy(['cart' => $cart->getId(), 'sku' => $sku]);

        EntityManager::remove($cart);
        EntityManager::flush();

        return $cart;
    }

    /**
     * @param $oldCart
     * @param $newCart
     */
    public function updateUser($oldCart, $newCart)
    {
        $items = $this->findByCartId($oldCart->getId());
        /** @var CartItem $item */
        foreach ($items as $item)
        {
            $item->setCart($newCart);
            EntityManager::persist($item);
            EntityManager::flush();
        }

        return $newCart;
    }

    /**
     * @param int $cartId
     * @return array
     */
    public function findByCartId($cartId)
    {
        return $this->findBy(['cart' => $cartId]);
    }

    /**
     * @param array $items
     * @return array
     */
    public function deleteList($items)
    {
        foreach ($items as $item) {
            EntityManager::remove($item);
            EntityManager::flush();
        }
    }
}
