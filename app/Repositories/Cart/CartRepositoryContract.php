<?php

namespace App\Repositories\Cart;

interface CartRepositoryContract
{
    public function create($userId, $sessionId);
    public function findUserId(int $userId);
    public function findSessionId(string $sessionId);
    public function delete(int $cartId);
}