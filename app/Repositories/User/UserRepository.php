<?php

namespace App\Repositories\User;

use App\Entities\User;
use Doctrine\ORM\EntityRepository;
use LaravelDoctrine\ORM\Facades\EntityManager;

class UserRepository extends EntityRepository implements UserRepositoryContract
{
    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function create(string $name, string $email, string $password)
    {
        $user = new User();
        $user->setData($name, $email, $password);

        EntityManager::persist($user);
        EntityManager::flush();

        return $user;
    }

    /**
     * @param $userId
     * @return null|object
     */
    public function getById($userId)
    {
        return $this->find($userId);
    }
}
