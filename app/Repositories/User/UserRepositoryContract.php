<?php

namespace App\Repositories\User;

interface UserRepositoryContract
{
    public function create(string $name, string $email, string $password);
    public function getById($userId);
}
