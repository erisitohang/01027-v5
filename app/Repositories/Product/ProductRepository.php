<?php

namespace App\Repositories\Product;

use Doctrine\ORM\EntityRepository;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use LaravelDoctrine\ORM\Pagination\PaginatesFromParams;

class ProductRepository extends EntityRepository implements ProductRepositoryContract
{

    use PaginatesFromParams;

    /**
     * @param int $limit
     * @param int $page
     * @return LengthAwarePaginator
     */
    public function all(int $limit, int $page) : LengthAwarePaginator
    {
        return $this->paginateAll($limit, $page);
    }

    /**
     * @param string $sku
     * @return null|object
     */
    public function findSku(string $sku)
    {
        return $this->findOneBy(['sku' => $sku]);
    }

    /**
     * @param array $list
     * @return mixed
     */
    public function findBySkuList(array $list)
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->andWhere('p.sku IN (:sku)')
            ->setParameter('sku', $list)
            ->getQuery()
            ->execute();
    }
}
