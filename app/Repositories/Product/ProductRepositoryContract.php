<?php

namespace App\Repositories\Product;

use \Doctrine\ORM\EntityManager;
use \App\Entities\Product;

interface ProductRepositoryContract
{
    public function all(int $limit, int $page);
    public function findSku(string $sku);
    public function findBySkuList(array $list);
}
