<?php

namespace App\Repositories\Order;

use App\Entities\Order;
use App\Entities\Product;

interface OrderItemRepositoryContract
{
    public function create(Order $order, Product $product, int $quantity, float $price);
}
