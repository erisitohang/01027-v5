<?php

namespace App\Repositories\Order;

use App\Entities\User;

interface OrderRepositoryContract
{
    public function create(User $user, float $total, $paid = false, $shipped = false);
    public function findById($orderId, $userId);
    public function setAsPaid($order);
    public function all($userId, $limit, $page);
}