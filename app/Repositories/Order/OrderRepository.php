<?php

namespace App\Repositories\Order;

use App\Entities\Order;
use App\Entities\User;
use Doctrine\ORM\EntityRepository;
use LaravelDoctrine\ORM\Facades\EntityManager;
use LaravelDoctrine\ORM\Pagination\PaginatesFromParams;

class OrderRepository extends EntityRepository implements OrderRepositoryContract
{
    use PaginatesFromParams;

    /**
     * @param User $user
     * @param float $total
     * @param int $paid
     * @param int $shipped
     * @return Order
     */
    public function create(User $user, float $total, $paid = 0, $shipped = 0)
    {
        $cart = new Order(
            $user,
            $total,
            $paid,
            $shipped
        );

        EntityManager::persist($cart);
        EntityManager::flush();

        return $cart;
    }

    public function findById($orderId, $userId)
    {
        return $this->findOneBy(['id' => $orderId, 'user' => $userId]);
    }

    /**
     * @param Order $order
     */
    public function setAsPaid($order)
    {
        $order->setPaid(true);
        EntityManager::persist($order);
        EntityManager::flush();
    }

    /**
     * @param $userId
     * @param $limit
     * @param $page
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function all($userId, $limit, $page)
    {
        $query = $this->createQueryBuilder('o')
            ->select('o')
            ->andWhere('o.user = :userId')
            ->setParameter('userId', $userId)
            ->getQuery();

        return $this->paginate($query, $limit, $page);
    }
}
