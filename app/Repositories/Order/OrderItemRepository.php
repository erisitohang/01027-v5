<?php

namespace App\Repositories\Order;

use App\Entities\Order;
use App\Entities\OrderItem;
use App\Entities\Product;
use Doctrine\ORM\EntityRepository;
use LaravelDoctrine\ORM\Facades\EntityManager;

class OrderItemRepository extends EntityRepository implements OrderItemRepositoryContract
{
    public function create(Order $order, Product $product, int $quantity, float $price)
    {
        $orderItem = new OrderItem(
            $order,
            $product,
            $quantity,
            $price
        );

        EntityManager::persist($orderItem);
        EntityManager::flush();

        return $orderItem;
    }
}
