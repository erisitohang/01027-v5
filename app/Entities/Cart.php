<?php

namespace App\Entities;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="carts")
 */
class Cart
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $session_id;

    /**
     * @ORM\OneToMany(targetEntity="CartItem", mappedBy="cart")
     * @var ArrayCollection|CartItem[]
     */
    private $cartItems;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    public function __construct($userId, $sessionId)
    {
        $now = Carbon::now();
        $this->user_id = $userId;
        $this->session_id = $sessionId;
        $this->createdAt = $now;
        $this->updatedAt = $now;
        $this->cartItems = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->session_id;
    }

    /**
     * @return CartItem[]|ArrayCollection
     */
    public function getCartItems()
    {
        return $this->cartItems;
    }
}
