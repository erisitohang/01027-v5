<?php

namespace App\Entities;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Support\Facades\Log;

/**
 * @ORM\Entity
 * @ORM\Table(name="cart_items")
 */
class CartItem
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="cart_item")
     * @var Cart
     */
    private $cart;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $sku;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * CartItem constructor.
     *
     * @param Cart $cart
     * @param string $sku
     * @param int $quantity
     */
    public function __construct($cart, $sku, $quantity)
    {
        $now = Carbon::now();
        $this->cart = $cart;
        $this->sku = $sku;
        $this->quantity = $quantity;
        $this->createdAt =$now;
        $this->updatedAt = $now;
    }

    /**
     * @param Cart $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }
}
