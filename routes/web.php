<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'home', 'uses' => 'HomeController@index'
]);

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('user', [
        'as' => 'dashboard', 'uses' => 'UserController@index'
    ]);
    Route::get('checkout', [
        'as' => 'checkout.checkout', 'uses' => 'OrderController@checkout'
    ]);
    Route::post('checkout', [
        'as' => 'checkout.store', 'uses' => 'OrderController@store'
    ]);
    Route::get('order/show/{id}', [
        'as' => 'order.show', 'uses' => 'OrderController@show'
    ]);

    Route::post('order/payment', [
        'as' => 'order.payment', 'uses' => 'OrderController@payment'
    ]);
    Route::get('order', [
        'as' => 'order.index', 'uses' => 'OrderController@index'
    ]);
});


Route::group(['middleware' => 'csrf'], function () {
    Route::post('cart/item/add', [
        'as' => 'cart.item.add', 'uses' => 'CartController@addItem'
    ]);
    Route::get('cart', [
        'as' => 'cart', 'uses' => 'CartController@index'
    ]);
});