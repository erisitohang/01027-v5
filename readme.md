OFFLINE CODE TESTING 01027-v5
====================


Server Requirements
================================

1. PHP >= 7.0.0
2. OpenSSL PHP Extension
3. PDO PHP Extension
4. Mbstring PHP Extension
5. Tokenizer PHP Extension
6. XML PHP Extension
7. Database


Server Requirements
================================

1.Clone repository
```sh
git clone https://erisitohang@bitbucket.org/erisitohang/01027-v5.git		
```
2.Go to root code folder, and run composer
```sh
composer install		
```
3.Copy .env.example to .env

#####Steps 4&5 you might not need to follow.
4.crate sqlite file
```$ssh
touch database/database.sqlite
```
5.update database configuration in .env file
```$ssh
DB_CONNECTION=sqlite
DB_DATABASE=/{ROOTPATH}/database/database.sqlite
```
6.Run migration
```sh
php artisan migrate		
```
7.Run doctrine update 
```sh
php artisan doctrine:schema:update	
```
8.Run data seeds (products table seeder) 
```sh
php artisan db:seed	
```
9.Generate Key
```sh
php artisan key:generate
```
10.Add stripe key into .env file
```sh
STRIPE_KEY=pk_test_XXXXX
STRIPE_SECRET=sk_test_XXXXX
```
10.Run server
```sh
php artisan serve
```