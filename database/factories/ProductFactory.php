<?php

if (!function_exists('randDate')) {
    function randDate()
    {
        return \Carbon\Carbon::now()
            ->subDays(rand(1, 100))
            ->subHours(rand(1, 23))
            ->subMinutes(rand(1, 60));
    }
}

/** @var \LaravelDoctrine\ORM\Testing\Factory $factory */
$factory->define(App\Entities\Product::class, function(Faker\Generator $faker) {
    $createdAt = randDate();
    $file = $faker->image('public/images/product',400,300, 'fashion', false);
    $path ='images/product/' . $file;
    return [
        'name' => $faker->sentence(3),
        'sku' => $faker->randomLetter . $faker->randomNumber(),
        'price' => $faker->randomNumber(2),
        'description' => $faker->text,
        'picture' => $path,
        'quantity' => rand(100, 999),
        'createdAt' => $createdAt,
        'updatedAt' => $createdAt
    ];
});
